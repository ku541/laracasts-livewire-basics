<?php

namespace App\Http\Livewire;

use App\Models\User;
use Livewire\Component;
use Livewire\WithPagination;

class DataTable extends Component
{
    use WithPagination;

    public $active = true;
    public $search;
    public $column = 'id';
    public $direction = 'desc';

    protected $queryString = [
        'search',
        'active' => ['except' => true],
        'column' => ['except' => 'id'],
        'direction' => ['except' => 'desc']
    ];

    public function sortBy($column, $direction)
    {
        $this->column = $column;
        $this->direction = $direction;
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        $users = User::active($this->active)->where(function ($query) {
            $query->where('name', 'like', "%$this->search%")
            ->orWhere('email', 'like', "%$this->search%");
        })->orderBy($this->column, $this->direction)->paginate(15);

        return view('livewire.data-table', compact('users'));
    }

    // public function paginationView()
    // {
    //     return 'livewire.custom-pagination-links-view';
    // }
}
