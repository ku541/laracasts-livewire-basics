<?php

namespace App\Http\Livewire;

use App\Models\Post;
use Livewire\Component;
use Livewire\WithFileUploads;

class EditPost extends Component
{
    use WithFileUploads;

    public $post;
    public $title;
    public $content;
    public $photo;
    public $temporaryUrl;

    protected $rules = [
        'title' => 'required|string|max:255',
        'content' => 'required|string|max:65535',
        'photo' => 'nullable|sometimes|image'
    ];

    public function mount(Post $post)
    {
        $this->post = $post;
        $this->title = $post->title;
        $this->content = $post->content;
    }

    public function updatedPhoto()
    {
        try {
            $this->temporaryUrl = $this->photo->temporaryUrl();
        } catch (\Throwable $th) {
            $this->temporaryUrl = '';
        }

        $this->validate(['photo' => 'image']);
    }

    public function submit()
    {
        $this->validate();

        $this->post->update([
            'title' => $this->title,
            'content' => $this->content
        ]);

        if ($this->photo) {
            $this->post->addMedia($this->photo->getRealPath())
                ->toMediaCollection('cover_photo');

            $this->photo = null;
        }

        session()->flash('success_message', 'Post was updated successfully!');
    }

    public function render()
    {
        return view('livewire.edit-post');
    }
}
