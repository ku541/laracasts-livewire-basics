<?php

namespace App\Http\Livewire;

use App\Models\Order;
use Livewire\Component;

class RevenuePoll extends Component
{
    public $revenue;

    public function mount()
    {
        $this->setRevenue();
    }

    public function render()
    {
        return view('livewire.revenue-poll');
    }

    public function setRevenue()
    {
        $this->revenue = Order::sum('price');
    }
}
