<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Mail\InquiryReceived;
use Illuminate\Support\Facades\Mail;

class ContactForm extends Component
{
    public $name;
    public $email;
    public $phone;
    public $message;

    protected $rules = [
        'name' => 'required',
        'email' => 'required|email',
        'phone' => 'required',
        'message' => 'required|min:5',
    ];

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function submit()
    {
        $this->validate();

        $contact['name'] = $this->name;
        $contact['email'] = $this->email;
        $contact['phone'] = $this->phone;
        $contact['message'] = $this->message;

        Mail::to('admin@example.com')->send(new InquiryReceived($contact));

        $this->reset();

        session()->flash('success_message', 'We received your message successfully and will get back to you shortly!');
    }


    public function render()
    {
        return view('livewire.contact-form');
    }
}
