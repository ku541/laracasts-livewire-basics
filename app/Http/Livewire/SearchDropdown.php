<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\Http;
use Livewire\Component;

class SearchDropdown extends Component
{
    public $search;
    public $searchResults = [];

    public function updatedSearch($value)
    {
        if (strlen($this->search) < 3) {
            return $this->searchResults = [];
        }

        $response = Http::get("https://itunes.apple.com/search/?term=$this->search&limit10");

        $this->searchResults = $response->json()['results'];
    }

    public function render()
    {
        return view('livewire.search-dropdown');
    }
}
