<?php

namespace App\Http\Livewire;

use App\Models\Post;
use Livewire\Component;

class CommentsSection extends Component
{
    public $post;
    public $comment;

    protected $rules = [
        'post' => 'required',
        'comment' => 'required|string|min:4|max:65535'
    ];

    public function mount(Post $post)
    {
        $this->post = $post;
    }

    public function postComment()
    {
        $this->validate();

        $this->post->comments()->create([
            'content' => $this->comment, 'username' => 'Guest'
        ]);

        $this->post->refresh();

        session()->flash('success_message', 'Comment was posted!');
    }

    public function render()
    {
        return view('livewire.comments-section');
    }
}
