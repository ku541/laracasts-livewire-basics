<?php

namespace App\Http\Livewire;

use App\Models\Tag;
use Livewire\Component;

class AddTags extends Component
{
    public $tags;

    protected $listeners = ['tagAdded', 'tagRemoved'];

    public function mount()
    {
        $this->tags = Tag::pluck('name');
    }

    public function tagAdded($tag)
    {
        Tag::create(['name' => $tag]);

        $this->emit('tagCreated', $tag);
    }

    public function tagRemoved($tag)
    {
        Tag::where('name', $tag)->delete();
    }

    public function render()
    {
        return view('livewire.add-tags');
    }
}
