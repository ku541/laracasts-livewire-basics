<?php

namespace App\Http\Controllers;

use App\Models\Post;

class PostController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return view('posts.show', compact('post'));
    }

    /**
     * Edit the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        return view('posts.edit', compact('post'));
    }

    /**
     * Update the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Post $post)
    {
        $validated = request()->validate([
            'title' => 'required|string|max:255',
            'content' => 'required|string|max:65535',
            'cover_photo' => 'sometimes|image'
        ]);

        if (! empty($validated['cover_photo'])) {
            $post->addMediaFromRequest('cover_photo')
                ->toMediaCollection('cover_photo');

            unset($validated['cover_photo']);
        }

        $post->update($validated);

        return redirect()->route('posts.show', $post);
    }
}
