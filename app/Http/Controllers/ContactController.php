<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\InquiryReceived;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function store(Request $request)
    {
        $contact = $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'message' => 'required',
        ]);

        Mail::to('admin@example.com')->send(new InquiryReceived($contact));

        return back()->with('success_message', 'We received your message successfully and will get back to you shortly!');
    }
}
