@extends('layouts.app')

@section('content')
    <div>
        <h2 class="text-4xl">{{ $post->title }}</h2>

        <img src="{{ $post->getFirstMediaUrl('cover_photo') }}">

        <div class="mt-8">
            {{ $post->content }}
        </div>

        <hr>

        <livewire:comments-section :post="$post"/>
    </div>
@endsection