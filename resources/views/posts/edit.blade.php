@extends('layouts.app')

@section('content')
    <livewire:edit-post :post="$post" />
@endsection