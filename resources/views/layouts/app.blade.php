<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <link href="{{ mix('css/app.css') }}" rel="stylesheet">

        @livewireStyles

        <title>Livewire Examples</title>

        <script defer src="https://unpkg.com/taggle@1.15.0/src/taggle.js"></script>
        <script defer src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js"></script>
    </head>

    <body>
        <main class="container mx-auto">
            @yield('content')
        </main>

        @livewireScripts
    </body>
</html>