<div
    class="border px-4 py-2 w-1/2"
    wire:ignore
    x-init="
        new Taggle($el, {
            tags: {{ json_encode($tags) }},

            onTagAdd: function(e, tag) {
                Livewire.emit('tagAdded', tag);
            },

            onTagRemove: function(event, tag) {
                Livewire.emit('tagRemoved', tag);
            }
        })

        Livewire.on('tagCreated', tag => {
            alert('A tag was created with the name of: ' + tag);
        })
    ">
</div>