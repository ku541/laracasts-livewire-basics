<?php

use App\Models\Post;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\PostCommentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('examples', ['posts' => Post::all()]);
});

Route::post('/contact', [ContactController::class, 'store']);
Route::resource('posts', PostController::class)->only('show', 'edit', 'update');
Route::resource('posts.comments', PostCommentController::class)->only('store');