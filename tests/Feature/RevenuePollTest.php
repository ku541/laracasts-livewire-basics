<?php

namespace Tests\Feature;

use App\Http\Livewire\RevenuePoll;
use App\Models\Order;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Livewire\Livewire;
use Tests\TestCase;

class RevenuePollTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function homepage_contains_revenue_poll_livewire_component()
    {
        $this->get('/')->assertSeeLivewire('revenue-poll');
    }

    /** @test */
    public function revenue_poll_sums_orders_correctly()
    {
        $orders = Order::factory()->count(2)->create();

        Livewire::test(RevenuePoll::class)
            ->assertSet('revenue', $orders->sum('price'))
            ->assertSee('$' . $orders->sum('price'));

        Order::factory()->count(2)->create();

        $revenue = Order::sum('price');

        Livewire::test(RevenuePoll::class)
            ->assertSet('revenue', $revenue)
            ->assertSee('$' . $revenue);
    }
}
