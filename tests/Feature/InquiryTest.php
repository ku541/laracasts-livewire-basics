<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Livewire\Livewire;
use App\Http\Livewire\ContactForm;
use App\Mail\InquiryReceived;
use Illuminate\Support\Facades\Mail;

class InquiryTest extends TestCase
{
    /** @test */
    public function homepage_contains_contact_form_livewire_component()
    {
        $this->get('/')->assertSeeLivewire('contact-form');
    }

    /** @test */
    public function contact_form_sends_out_an_email()
    {
        Mail::fake();

        Livewire::test(ContactForm::class)->set('name', 'John Doe')
            ->set('email', 'john@example.com')
            ->set('phone', '0777777777')
            ->set('message', 'Hello World')
            ->call('submit')
            ->assertSee('We received your message successfully and will get back to you shortly!');
        
        Mail::assertSent(InquiryReceived::class, function ($mail) {
            $mail->build();

            return $mail->hasTo('admin@example.com') &&
                $mail->from('john@example.com') &&
                $mail->subject('Inquiry Received');
        });
    }

    /** @test */
    public function contact_form_name_field_is_required()
    {
        Mail::fake();

        Livewire::test(ContactForm::class)->set('email', 'john@example.com')
            ->set('phone', '0777777777')
            ->set('message', 'Hello World')
            ->call('submit')
            ->assertHasErrors(['name' => 'required']);
    }

    /** @test */
    public function contact_form_message_field_has_minimum_characters()
    {
        Mail::fake();

        Livewire::test(ContactForm::class)->set('email', 'john@example.com')
            ->set('message', 'Test')
            ->call('submit')
            ->assertHasErrors(['message' => 'min']);
    }
}
