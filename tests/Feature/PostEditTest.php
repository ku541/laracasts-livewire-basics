<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Post;
use Livewire\Livewire;
use App\Http\Livewire\EditPost;
use Illuminate\Http\UploadedFile;

class PostEditTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function edit_post_page_contains_edit_post_livewire_component()
    {
        $post = Post::factory()->create();

        $this->get(route('posts.edit', $post))
            ->assertSeeLivewire('edit-post');
    }

    /** @test */
    public function edit_post_page_form_works()
    {
        $post = Post::factory()->create();

        Livewire::test(EditPost::class, ['post' => $post])
            ->set('title', 'Title')->set('content', 'Content')->call('submit')
            ->assertSee('Post was updated successfully!');

        $post->refresh();

        $this->assertEquals('Title', $post->title);
        $this->assertEquals('Content', $post->content);
    }

    /** @test */
    public function edit_post_page_upload_works_for_images()
    {
        $post = Post::factory()->create();

        $photo = UploadedFile::fake()->image('photo.jpg');

        Livewire::test(EditPost::class, ['post' => $post])
            ->set('title', 'Title')->set('content', 'Content')
            ->set('photo', $photo)
            ->call('submit')
            ->assertSee('Post was updated successfully!');
    }

    /** @test */
    public function edit_post_page_upload_does_not_work_for_non_images()
    {
        $post = Post::factory()->create();

        $photo = UploadedFile::fake()->create('doc.pdf');

        Livewire::test(EditPost::class, ['post' => $post])
            ->set('title', 'Title')->set('content', 'Content')
            ->set('photo', $photo)
            ->call('submit')
            ->assertHasErrors(['photo' => 'image'])
            ->assertSee('The photo must be an image');
    }
}
