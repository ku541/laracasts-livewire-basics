<?php

namespace Tests\Feature;

use App\Http\Livewire\CommentsSection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Post;
use Livewire\Livewire;

class CommentSectionTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function homepage_contains_posts()
    {
        $post = Post::factory()->create();

        $this->get('/')->assertSee($post->title);
    }

    /** @test */
    public function post_page_contains_comments_section_livewire_component()
    {
        $post = Post::factory()->create();

        $this->get(route('posts.show', $post))
            ->assertSeeLivewire('comments-section');
    }

    /** @test */
    public function valid_comment_can_be_posted()
    {
        $post = Post::factory()->create();

        Livewire::test(CommentsSection::class, ['post' => $post])
            ->set('post', $post)->set('comment', 'Valid comment')
            ->call('postComment')->assertSee('Comment was posted!')
            ->assertSee('Valid comment');
    }

    /** @test */
    public function comment_is_required()
    {
        $post = Post::factory()->create();

        Livewire::test(CommentsSection::class, ['post' => $post])
            ->set('post', $post)->call('postComment')
            ->assertHasErrors(['comment' => 'required'])
            ->assertSee('The comment field is required.');
    }

    /** @test */
    public function comment_requires_min_characters()
    {
        $post = Post::factory()->create();

        Livewire::test(CommentsSection::class, ['post' => $post])
            ->set('post', $post)->set('comment', 'Hmm')
            ->call('postComment')
            ->assertHasErrors(['comment' => 'min'])
            ->assertSee('The comment must be at least 4 characters.');
    }
}
