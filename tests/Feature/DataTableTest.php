<?php

namespace Tests\Feature;

use App\Http\Livewire\DataTable;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use Livewire\Livewire;

class DataTableTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function homepage_contains_data_table_livewire_component()
    {
        $this->get('/')->assertSeeLivewire('data-table');
    }

    /** @test */
    public function data_table_active_checkbox_works_correctly()
    {
        $activeUser = User::factory()->active(true)->create();

        $inactiveUser = User::factory()->active(false)->create();

        Livewire::test(DataTable::class)
            ->assertSee($activeUser->name)
            ->assertDontSee($inactiveUser->name)
            ->set('active', false)
            ->assertSee($inactiveUser->name)
            ->assertDontSee($activeUser->name);
    }

    /** @test */
    public function data_table_searches_name_correctly()
    {
        $foo = User::factory()->create(['name' => 'Foo']);

        $bar = User::factory()->create(['name' => 'Bar']);

        Livewire::test(DataTable::class)
            ->set('search', 'Foo')
            ->assertSee($foo->name)
            ->assertDontSee($bar->name);
    }

    /** @test */
    public function data_table_searches_email_correctly()
    {
        $foo = User::factory()->create(['email' => 'foo@example.com']);

        $bar = User::factory()->create(['email' => 'bar@example.com']);

        Livewire::test(DataTable::class)
            ->set('search', 'Foo')
            ->assertSee($foo->name)
            ->assertDontSee($bar->name);
    }

    /** @test */
    public function data_table_sorts_name_ascending_correctly()
    {
        $users = User::factory()->count(5)->create();

        Livewire::test(DataTable::class)
            ->call('sortBy', 'name', 'asc')
            ->assertSeeInOrder($users->pluck('name')->sort()->all());
    }

    /** @test */
    public function data_table_sorts_name_descending_correctly()
    {
        $users = User::factory()->count(5)->create();

        Livewire::test(DataTable::class)
            ->call('sortBy', 'name', 'desc')
            ->assertSeeInOrder($users->pluck('name')->sortDesc()->all());
    }

    /** @test */
    public function data_table_sorts_email_ascending_correctly()
    {
        $users = User::factory()->count(5)->create();

        Livewire::test(DataTable::class)
            ->call('sortBy', 'email', 'asc')
            ->assertSeeInOrder($users->pluck('email')->sort()->all());
    }

    /** @test */
    public function data_table_sorts_email_descending_correctly()
    {
        $users = User::factory()->count(5)->create();

        Livewire::test(DataTable::class)
            ->call('sortBy', 'email', 'desc')
            ->assertSeeInOrder($users->pluck('email')->sortDesc()->all());
    }
}
