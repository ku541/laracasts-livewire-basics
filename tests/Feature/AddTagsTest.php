<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Tag;
use Livewire\Livewire;
use App\Http\Livewire\AddTags;

class AddTagsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function homepage_contains_add_tags_livewire_component()
    {
        $this->get('/')->assertSeeLivewire('add-tags');
    }

    /** @test */
    public function loads_existing_tags_correctly()
    {
        $tags = Tag::factory()->count(2)->create();

        Livewire::test(AddTags::class)
            ->assertSet('tags', json_encode($tags->pluck('name')));
    }

    /** @test */
    public function adds_tags_correctly()
    {
        Livewire::test(AddTags::class)->emit('tagAdded', 'new')
            ->assertEmitted('tagCreated', 'new');

        $this->assertDatabaseHas('tags', ['name' => 'new']);
    }

    /** @test */
    public function removes_tags_correctly()
    {
        $tag = Tag::factory()->create();

        Livewire::test(AddTags::class)->emit('tagRemoved', $tag->name);

        $this->assertDatabaseMissing('tags', $tag->only('name'));
    }
}
